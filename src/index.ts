import { parallel, reflect, waterfall } from "async";
import { createReadStream, lstat, Stats } from "fs";
import { archive } from "src/constants/paths.json";
import { pack } from "tar-stream";
import { toGzip } from "./lib/archive";
import applyToDirectory, { onPathFn } from "./lib/fs/directory";
import { fromFile, toFile } from "./lib/fs/streamFile";
import { Callback } from "./lib/types/definition";
import { logger } from "./services/logger";

const outputPath = archive.to.custom ? archive.to.custom : archive.to.default;
const archivables =
  archive.from.custom && archive.from.custom.length > 0
    ? archive.from.custom
    : archive.from.default;

const toLog = (err: Error, res: any) => {
  if (err) console.error(err);
  console.info(res);
};

const door = (path: string, cb: Callback) => {
  lstat(path, (err, stats) => {
    if (err) return cb(err);
    if (stats.isDirectory()) applyToDirectory(path, door, cb);
    else cb(/*null, console.info(path)*/);
  });
};

applyToDirectory(archivables[0], door, toLog);

// (p: string[]) => string => tar

