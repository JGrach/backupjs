import { expect } from "chai";
import proxyquire from "proxyquire";
import { SinonSpy, SinonStub, spy, stub } from "sinon";
import { Callback } from "../types/definition";
import { onEndFn, onPathFn } from "./directory";

const fsMocked = {
  readdir: stub()
};

const dir = proxyquire("./directory.ts", {
  fs: fsMocked
});

describe("testing fs/lib/directory functions", () => {
  describe("testing applyToDirectory", () => {
    let onPath: SinonSpy;
    let applyToSubpathes: SinonStub;

    before(() => {
      onPath = spy();
      applyToSubpathes = stub(dir, "applyToSubpathes").callsArg(3);
    });

    it("should fire callback with error on readdir return error", done => {
      const tp = Date.now().toString();
      const path = "whatever";
      fsMocked.readdir.callsArgWith(1, new Error(tp.slice(0)));

      const onEnd: onEndFn = err => {
        expect(applyToSubpathes.notCalled).equal(true);
        expect(err && err.message).equal(tp);
        done();
      };

      dir.applyToDirectory(path.slice(0), onPath, onEnd);
    });

    it("should fire callback with reports from applyToSubpathes", done => {
      const path = "whatever";
      const report = ["this", "is", "an", "error", "array"];
      fsMocked.readdir.callsArgWith(1, null, null);
      applyToSubpathes.callsArgWith(3, null, report.slice(0));

      const onEnd: onEndFn = (_, result) => {
        expect(applyToSubpathes.calledOnce).equal(true);
        expect(result.join()).equal(report.join());
        done();
      };

      dir.applyToDirectory(path.slice(0), onPath, onEnd);
    });

    afterEach(() => {
      onPath.resetHistory();
      fsMocked.readdir.reset();
      applyToSubpathes.reset();
    });

    after(() => {
      applyToSubpathes.restore();
    });
  });

  describe("testing applyToSubpathes", () => {
    let onPath: SinonStub;

    before(() => {
      onPath = stub();
    });

    it("should fire onPath for each supathes", done => {
      const path = "whatever";
      const subpathes = ["one", "two", "three"];
      onPath.callsFake((p: string, cb: Callback) => cb(null, p));

      const onEnd: onEndFn = err => {
        const onPathCalls = onPath.getCalls();
        expect(onPathCalls).lengthOf(subpathes.length);

        const onPathCalledWith = onPathCalls.map(call => {
          expect(call.args).lengthOf(2);
          return call.args[0] as string;
        });

        subpathes.forEach(subpath => {
          expect(onPathCalledWith).include(`${path}/${subpath}`);
        });

        done();
      };

      dir.applyToSubpathes(path.slice(0), subpathes.slice(0), onPath, onEnd);
    });

    it("should fire callback with a report error on onPath error", done => {
      const path = "whatever";
      const subpathes = ["one", "two", "three"];
      const tp = Date.now().toString();
      onPath.onFirstCall().callsArgWith(1, new Error(`0: ${tp.slice(0)}`));
      onPath.onSecondCall().callsArgWith(1, null, null);
      onPath
        .onThirdCall()
        .callsArgWith(1, null, [new Error(`3: ${tp.slice(0)}`)]);

      const onEnd: onEndFn = (err, res) => {
        // tslint:disable-next-line:no-unused-expression
        expect(err).null;
        expect(res).lengthOf(2);
        expect(res[0].message).equal(`0: ${tp}`);
        expect(res[1].message).equal(`3: ${tp}`);
        done();
      };

      dir.applyToSubpathes(path.slice(0), subpathes.slice(0), onPath, onEnd);
    });

    afterEach(() => {
      onPath.reset();
    });
  });
});
