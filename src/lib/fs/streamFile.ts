import { createReadStream, createWriteStream } from "fs";
import { Callback } from "../types/definition";

export const toFile = (destination: string, cb?: Callback) => {
  const out = createWriteStream(destination);

  if (cb) {
    out.on("error", cb);
    out.on("close", () => cb(null, `File ${destination} successfully writed`));
  }

  return out;
};

export const fromFile = (source: string, cb?: Callback) => {
  const from = createReadStream(source);

  if (cb) {
    from.on("error", cb);
    from.on("close", () => cb(null, `File ${source} successfully readed`));
  }

  return from;
};
