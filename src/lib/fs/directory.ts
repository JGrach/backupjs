import { parallel, reflectAll, waterfall } from "async";
import { readdir } from "fs";
import { Callback } from "../types/definition";

export type onPathFn = (path: string, cb: Callback) => void;
export type onEndFn = (err: Error | void, results: Error[]) => void;

export const applyToSubpathes = (
  path: string,
  subpathes: string[],
  onPath: onPathFn,
  onEnd: onEndFn
) => {
  parallel(
    reflectAll<Error[]>(
      subpathes.map(subpath => cb => {
        onPath(`${path}/${subpath}`, cb);
      })
    ),
    (_, results) => {
      const reports = results.reduce((acc, { value, error }) => {
        if (error) return [...acc, error];
        else if (value) return [...acc, ...value];
        else return acc;
      }, []);
      onEnd(_, reports);
    }
  );
};

export const applyToDirectory = (
  path: string,
  onPath: onPathFn,
  onEnd: onEndFn
) => {
  waterfall(
    [
      (cb: Callback) => readdir(path, cb),
      (subpathes: string[], cb: Callback) =>
        applyToSubpathes(path, subpathes, onPath, cb)
    ],
    onEnd
  );
};

export default applyToDirectory;
