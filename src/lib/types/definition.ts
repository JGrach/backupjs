export type Callback = (err: Error | void, ...args: any[]) => any;
