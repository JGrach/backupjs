import { createGzip } from "zlib";
import { Callback } from "../types/definition";

const toGzip = (cb: Callback) => {
  const gzip = createGzip({ level: 9 });

  gzip.on("error", cb);
  gzip.on("close", () => cb(null, "Zlib successfully close"));

  return gzip;
};

export default toGzip;
