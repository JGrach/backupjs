import console, { Logger as LoggerType } from "./console";

export const logger = console;
export type Logger = LoggerType;
