import { createLogger, format, Logger, transports } from "winston";

const { NODE_ENV = "dev" } = process.env;
const level = NODE_ENV !== "production" ? "silly" : "info";

const { combine, timestamp, json, metadata, errors, cli } = format;

const withErrors = format(info => {
  if (info.metadata && info.metadata.stack) {
    info.message = info.metadata.stack;
  }
  return info;
});

const logger: Logger = createLogger({
  format: combine(errors({ stack: true }), metadata(), timestamp(), json()),
  level,
  transports: [
    new transports.Console({ format: combine(withErrors(), cli()) }),
    new transports.File({
      filename: `logs/${NODE_ENV}/error.log`,
      level: "error"
    }),
    new transports.File({
      filename: `logs/${NODE_ENV}/combined.log`
    })
  ]
});

export default logger;
