/* Agnosticism : Map custom logger on common console method */

import { LeveledLogMethod, Logger as LoggerWinston, LogMethod } from "winston";
import winston from "./winston";

let counter = 0;

type countFn = (
  msg: string | object,
  level?: "debug" | "error" | "info" | "warn"
) => LoggerWinston;

const count: countFn = (msg, level = "info") => {
  const current = counter;
  counter++;
  if (typeof msg === "string") return winston[level](`${current}: ${msg}`);
  else
    return winston[level]({
      counter: current,
      ...msg
    });
};

export interface Logger {
  count: countFn;
  debug: LeveledLogMethod;
  error: LeveledLogMethod;
  info: LeveledLogMethod;
  log: LogMethod;
  warn: LeveledLogMethod;
}

const logger = {
  count,
  debug: winston.debug.bind(winston),
  error: winston.error.bind(winston),
  info: winston.info.bind(winston),
  log: winston.log.bind(winston),
  warn: winston.warn.bind(winston)
};

export default logger;
